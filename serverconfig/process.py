# import library
import requests
from skimage.io import imread, imshow, imsave
import pyclesperanto_prototype as cle

# define constants
urlPathRoot = "http://192.168.0.156:8000/images/"
urlPathFile = "http://192.168.0.156:8000/images/Tree_Rings.jpg" 
filenameOutput = "image.tif"

# function to download and save image file
def downSaveImageFile(urlPathFile, filenameOutput):
    # download file
    serverAnswer = requests.get(urlPathFile)
    # save result as file on local file system
    fileOutput = open(filenameOutput, "wb")
    fileOutput.write(serverAnswer.content)

# function to download, process and save image file
def downSaveProcImageFile(urlPathFile, filenameOutput):
    # download file
    image = imread(urlPathFile)
    # process file
    gr = cle.gradient_x(image)
    # save result as file on local file system
    imsave(filenameOutput, gr)

# function to upload 
def sendFile(urlPathRoot, filename):
    # specify file for uploading
    files = {'file': open(filename, 'rb')}
    # send file to http-server
    r = requests.post(urlPathRoot, files=files)

# download and handle imahe with saving result
downSaveProcImageFile(urlPathFile, filenameOutput)
# send processed file into server
sendFile(urlPathRoot, filenameOutput)