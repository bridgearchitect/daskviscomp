import http.server
import socketserver

PORT = 8000
DIRECTORY = "D:/Univerisity/TeamProject/daskviscomp/serverconfig/images/*"


class Handler(http.server.SimpleHTTPRequestHandler):
    def _init_(self, *args, **kwargs):
        super()._init_(*args, directory=DIRECTORY, **kwargs)
    
    def do_POST(self):
        
        content_length = int(self.headers['Content-Length'])
        file_data = self.rfile.read(content_length)

        with open('result.txt', "wb") as f:
            f.write(self)

        self.send_response(200)
        self.end_headers()
        self.wfile.write(b'File uploaded successfully')

with socketserver.TCPServer(("", PORT), Handler) as httpd:
    print("serving at port", PORT)
    httpd.serve_forever()