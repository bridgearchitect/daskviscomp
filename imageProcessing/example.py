import pyclesperanto_prototype as cle
from skimage.io import imread, imshow, imsave

# read image from Internet
image = imread("/home/bridgearchitect/Pictures/bridge.jpg")
# handle image
gx = cle.gradient_x(image)
# show processed image
imsave("result.tif", gx)
