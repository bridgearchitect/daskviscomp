# imageProcessing

This part will contain the code for image processing using pyclesperanto library.

## image.ipynb

Here we are using various functions of pyclesperanto to process an image and saving the processed image. Some of the functions used are:
gaussian_blur: Using a gauusian filter to create a smoother image.
connected_components_labeling_box: Adding label for every homogeneous domain in an image.
voronoi_otsu_labeling: Divide image on several domains using Voronoi diagrams
