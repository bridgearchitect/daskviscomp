import pyclesperanto_prototype as cle
from skimage.io import imshow, imread, imsave

cle.select_device("GTX")
# check which device is uses right now
print(cle.get_device())

# load data
image = imread('https://imagej.nih.gov/ij/images/2D_Gel.jpg')

# process the image
blurred = cle.gaussian_blur(image, sigma_x=2, sigma_y=2)
imshow(blurred)

# for debugging: save image to disc
imsave("result.tif", cle.pull(blurred))
