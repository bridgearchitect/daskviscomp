import pyclesperanto_prototype as cle
import matplotlib.pyplot as plt
from dask.distributed import Client
from skimage.io import imread, imshow, imsave

# define network constants
ipAddressHost = "192.168.0.186:52488"
# define image source from Internet
source = "https://imagej.nih.gov/ij/images/DentalRadiograph.png"

# define image function for calculation
def handleImage(source):
    image = imread(source)
    gr = cle.gradient_x(image)
    imsave("result.tif", gr)
    return True

# create client using ip addresses
client = Client(ipAddressHost)

# specify computational task
task = client.submit(handleImage, source)

# execute and print result
finalImage = task.result()
# print(finalImage)
imshow(finalImage)
# imsave("result.tif", finalImage)
