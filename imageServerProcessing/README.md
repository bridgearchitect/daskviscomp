# imageServerProcessing

This project acts as the client for the image processing using pyclesperanto.

## image.ipynb

In this file we have used pyclesperanto as the image processing library and have defined a simple handleImage function. This function is then executed in the dask server using the dask library.

## Usage

To use first we have to run the scheduler and the worker which are present in the daskServer project. After successful execution of the dask scheduler and worker you can run image.ipynb file.