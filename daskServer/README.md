# daskServer

The daskServer contains server files and configuration of the computational cluster for calculations of basic math operations e.g. summation of squares of whole numbers from 0-9999 written using the dask library.

## calc.py

This file acts as the client. It has to send query to perform a simple calculation of summation of squares as mentioned above.

## schedular.py

This piece of code will run on the scheduling machine and is solely responsible for the distributing the tasks incoming from the client machine to the workers.

## worker.py

This piece of code will be running on the worker machines and will be accepting the task given by the scheduler and will executes the given tasks and return the results.

## Usage

You can use the following steps to run the project for processing of the data: 

```bash
python scheduler.py
```
This command will start the scheduler on a specific port in the scheduler machine. You need to replace the ip and port of the scheduler in the worker.py file.

```bash
python worker.py
```
The aforementioned code will run the worker and will register it in the scheduler. You can run multiple schedulers to have parallel processing of the tasks.

```bash
calc.py
```
After the scheduler-worker is ready you can execute the client (calc.py in this case) which would send the task to the scheduler and the scheduler can now distribute it among the workers and return the results after the processing is completed.
