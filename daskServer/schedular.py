import asyncio
from dask.distributed import Scheduler, Worker

async def f():
    s = Scheduler()
    s = await s
    await s.finished()

asyncio.get_event_loop().run_until_complete(f())
