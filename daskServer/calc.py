# include modules
from dask.distributed import Client

# define network constants
ipAddressHost = "192.168.0.154:57545"

# define square function for math calculation
def square(x):
    return x ** 2

# create client using ip addresses
client = Client(ipAddressHost)

# specify computational task
square = client.map(square, range(10000))
task = client.submit(sum, square)

# execute task and print result
print(task.result())
