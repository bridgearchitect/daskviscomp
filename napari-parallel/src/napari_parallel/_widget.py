# import global libraries
from typing import TYPE_CHECKING
from magicgui import magic_factory
from qtpy.QtWidgets import QHBoxLayout, QVBoxLayout, QPushButton, QLabel, QLineEdit, QWidget
from pyqode.python.widgets import PyCodeEdit
from dask.distributed import Client
import glob
import copy

# verification of napari modules
if TYPE_CHECKING:
    import napari

# define constants
templateInput = "YYYY"
templateOutput = "XXXX"
pathSource = "P:/dataset"
pathOutput = "P:/results/"
begStringOutput = "result"
regExrp = "*.tif"
inputSubstring = "imread("
outputSubstring = "imsave("
viewerSubstring = "viewer"
ipAddressHost = "192.168.0.154:57556"

# create class for widget of parallel calculation
class ParallelQWidget(QWidget):

    # initialization of class
    def __init__(self, napari_viewer):

        # basic initialization
        super().__init__()
        self.viewer = napari_viewer

        # create button
        buttonProcess = QPushButton("Process")
        buttonProcess.clicked.connect(self._on_click)

        # specify horizontal layout for app
        self.setLayout(QHBoxLayout())
        # add button in app
        self.layout().addWidget(buttonProcess)
        # field for code of image processing
        self.codeProcessing = ""

        # specify vertical layout for app
        self.setLayout(QVBoxLayout())

        # add text field for schedular
        self.labelSched = QLabel('Schedular:', self)
        self.layout().addWidget(self.labelSched)
        self.textFieldSched = QLineEdit(self)
        self.layout().addWidget(self.textFieldSched)

    # function to execute code from string variable
    def executeCodeFromString(code):
        # execute code
        exec(code)
        # return default answer
        return True

    # handler for "process" button
    def _on_click(self):

        # find out all files for analysis
        allFiles = glob.glob(pathSource + "\\" + regExrp)

        # receive code of program
        code = self.codeProcessing
        # delete the extra rows of code
        rowsInter = code.rsplit("\n")
        i = 0
        while i < len(rowsInter):
            if viewerSubstring in rowsInter[i]:
                del rowsInter[i]
                i -= 1
            i += 1
        codeRed = "\n".join(rowsInter)

        # split code into independent rows
        rowsCode = codeRed.splitlines()
        # receive the last row
        lastRow = rowsCode[-1]
        # receive substrings using the last row
        substrings = lastRow.split(" ")
        # get name of variable using the first element of substrings
        nameVar = substrings[0]

        # add new code for saving results
        codeAdv = codeRed + "\n" + "from skimage.io import imsave" + "\n"
        codeAdv = codeAdv + "imsave(\"" + templateOutput + "\"," + nameVar + ")\n"

        # modify input part of program
        i = 0
        rowsInter = codeAdv.rsplit("\n")
        while i < len(rowsInter):
            if inputSubstring in rowsInter[i]:
                substrings = rowsInter[i].split("=")
                rowsInter[i] = substrings[0] + "= imread(\"" + templateInput + "\")"
                break
            i += 1
        codeAdv = "\n".join(rowsInter)

        # create dask-client
        client = Client(ipAddressHost)
        # initializate variables for saving codes for image processing
        codeTasks = [None] * len(allFiles)

        # go through all files
        for i in range(len(allFiles)):
            # copy code in specific variable
            codeExec = copy.deepcopy(codeAdv)
            # modify input part of program
            j = 0
            rowsInter = codeExec.rsplit("\n")
            while j < len(rowsInter):
                if inputSubstring in rowsInter[j]:
                    substrings = rowsInter[j].split("=")
                    rowsInter[j] = substrings[0] + "= imread(\"" + allFiles[i] + "\")"
                    break
                j += 1
            codeExec = "\n".join(rowsInter)
            # modify output part of program
            j = 0
            rowsInter = codeExec.rsplit("\n")
            while j < len(rowsInter):
                if outputSubstring in rowsInter[j]:
                    substrings = rowsInter[j].split(",")
                    rowsInter[j] = "imsave(\"" + pathOutput + begStringOutput + str(i) + ".tif\"," + nameVar + ")\n"
                    break
                j += 1
            codeExec = "\n".join(rowsInter)
            # submit tasks using Dask
            codeTasks[i] = codeExec

        # submit all tasks for execution
        task = client.submit(exec, codeTasks[0])
        # wait for all tasks to complete
        results = client.gather(task)
        # debug code
        print(results)


    # class method to create dask window
    @classmethod
    def get_dask_window(cls, viewer, create_editor=True, _for_testing=False):

        # initialization
        dask_window = None
        # create a new Dask window if requested
        if create_editor:
            # create window
            dask_window = ParallelQWidget(viewer)
            # add window to dock widget
            w = viewer.window.add_dock_widget(dask_window, area="right", name="Dask window")
            if not _for_testing:
                w.setFloating(True)
            # define size of window
            w.resize(400, 200)
            # return window
            return dask_window
        # default answer
        return dask_window
