import os

# define constants
filenameInput = "exampleCode.txt"

# function to execute code in string
def execuseCodeInString(code):
    # execute code
    exec(code)

# open file
fileInput = open(filenameInput, "r")
# read file as cose
code = fileInput.read()
# close file
fileInput.close()

# execute code from file
execuseCodeInString(code)
