# import global libraries
import re
import glob
import sys

# function to read all regular expressions
def readRegularExpression():
    # write message in terminal
    print("Regular expression for file:")
    # read data from terminal
    regExrp = sys.stdin.readline()
    # delete extra symbol in the end
    regExrp = regExrp[:-1]
    # return regular expression
    return regExrp

# function to find all files using regular expressions:
def findAllFiles(regExrp):
    # search for all files using regular expression
    allFiles = glob.glob(regExrp)
    # return list of found files
    return allFiles

# read regular expression from terminal
regExrp = readRegularExpression()
# find all files using regular expression
allFiles = findAllFiles(regExrp)
# debug code
print(allFiles)
