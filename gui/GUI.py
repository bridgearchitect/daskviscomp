import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QLineEdit, QPushButton, QVBoxLayout, QWidget
from PyQt5.QtGui import QPalette, QColor

class DaskGUI(QMainWindow):
    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):
        self.setWindowTitle('GUI for Dask Connection')
        self.setGeometry(100, 100, 300, 200)

        central_widget = QWidget(self)
        self.setCentralWidget(central_widget)

        palette = QPalette()
        palette.setColor(QPalette.Window, QColor(0, 0, 0))  # Black background color
        palette.setColor(QPalette.WindowText, QColor(255, 255, 255))  # White text color
        self.setPalette(palette)

        layout = QVBoxLayout()

        self.label = QLabel('Schedular:', self)
        layout.addWidget(self.label)

        self.text_field = QLineEdit(self)
        layout.addWidget(self.text_field)

        self.button = QPushButton('Process', self)
        self.button.clicked.connect(self.onButtonClick)
        layout.addWidget(self.button)

        central_widget.setLayout(layout)

    def onButtonClick(self):
        text = self.text_field.text()
        print(f'Entered Text: {text}')

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = DaskGUI()
    window.show()
    sys.exit(app.exec_())
