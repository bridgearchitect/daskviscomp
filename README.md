# daskVisComp

The main goal of the project daskVisComp (dask visual computation) is fast
parallel processing of high-resolution images weighing several or tens of gigabytes
using the [Python](https://www.python.org/) programming language, the [dask](https://www.dask.org/)
parallel computing library, and the image processing library
[pyclesperanto](https://github.com/clEsperanto/pyclesperanto_prototype).

The given project has the following folders:

1. daskSimpleComputation is simple project to calculate sum of two random vectors
using possibility of dask library.
2. daskServer is project to calculate sum $$ \sum_{n = 1}^{9999} n^2 $$ using the
remote dask scheduler together with workers.
3. imageProcessing is project to process biological [image](https://imagej.nih.gov/ij/images/blobs.gif)
from the Internet using pyclesperanto library (Gaussian blur, Voronoi labeling and so on).
4. gui is project to provide graphical user interface for specification of dask parameters such
as ip-address, tcp-port or list of all processed images 
5. napari-hello is basic project to specify plugin for napari image processing software namely
creation of one button to demonstrate message "Hello World" on display.
6. regExpr is project to specify all files for image processing using regular expression. The project
was written using Python programming language

## Problem

In the third project, image processing is done properly using functions ``voronoi_otsu_labeling()``,
``gaussian_blur()`` and ``threshold_otsu`` of pyclesperanto library. It is possible to see results
of the foregoing procedures using function ``imshow()``. However, saving the final result as an
image file (extension .png, .jpg or .tif) leads to a negative result (ie almost all pixels of the
image are black). Also program gives warning ``(name of file-image) is a low contrast image``
in this case.

## Environment

The developed software is demanding about Python library versions. If the client, scheduler, and
worker have different versions of the libraries, there is a high probability of a programming
error. We recommend using the library versions specified in the file ``environment.yml``i.
